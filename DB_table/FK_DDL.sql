alter table user
add
foreign key (post_no)
references post (post_no)
;

alter table post
add
foreign key (keyword_no)
references keyword (keyword_no)
;

alter table post
add
foreign key (user_id)
references user (user_id)
;

alter table like_post
add
foreign key (post_no)
references post (post_no)
;

alter table like_post
add
foreign key (user_id)
references user (user_id)
;

alter table comment
add
foreign key (post_no)
references post (post_no)
;

alter table comment
add
foreign key (user_id)
references user (user_id)
;

alter table follow
add
foreign key (from_id)
references user (user_id)
;

alter table follow
add
foreign key (to_id)
references user (user_id)
;

alter table message
add
foreign key (from_id)
references user (user_id)
;

alter table message
add
foreign key (to_id)
references user (user_id)
;