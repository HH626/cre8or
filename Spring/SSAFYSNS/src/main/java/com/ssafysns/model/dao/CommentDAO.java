package com.ssafysns.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafysns.model.dto.Comment;

@Mapper
public interface CommentDAO {
	public List<Comment> searchByPostno(int post_no);
	public void insert(Comment comment);
	public void update(Comment comment);
	public void delete(int comment_no);
}