package com.ssafysns.model.dao;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafysns.model.dto.Post;
import com.ssafysns.model.dto.postKeywordNCategory;
@Mapper
public interface PostDAO {
	public List<Post> searchAll();
	public List<Post> searchLimit(int limit);
	public List<Post> searchRankDaily(String date);
	public List<Post> searchbyKeyword(int keyword_no);
	public int searchKeywordandCategory(postKeywordNCategory KNC);
	public List<Post> searchByEmail(String user_email);
	public List<Post> searchSection(int section);
	public List<Post> searchSectionLimit(HashMap<String, Integer> map);
	public Post searchPostno(int post_no);
	public void addLike(int post_no);
	public void subLike(int post_no);
	public void insert(Post post);
	public void update(Post post);
	public void delete(int post_no);
}