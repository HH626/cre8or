package com.ssafysns.model.dto;

public class User {
	String user_email;
	String nickname;
	int post_no;
	String reg_date;
	String live;
	String profile_comment;
	String profile_img;
	
	public User() {
		super();
	}
	
	public User(String user_email, String nickname) {
		super();
		this.user_email = user_email;
		this.nickname = nickname;
	}

	public User(String user_email, String nickname, int post_no, String reg_date, String profile_comment) {
		super();
		this.user_email = user_email;
		this.nickname = nickname;
		this.post_no = post_no;
		this.reg_date = reg_date;
		this.profile_comment = profile_comment;
	}
	


	public String getProfile_comment() {
		return profile_comment;
	}

	public void setProfile_comment(String profile_comment) {
		this.profile_comment = profile_comment;
	}

	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getPost_no() {
		return post_no;
	}
	public void setPost_no(int post_no) {
		this.post_no = post_no;
	}
	public String getLive() {
		return live;
	}
	public void setLive(String live) {
		this.live = live;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getProfile_img() {
		return profile_img;
	}

	public void setProfile_img(String profile_img) {
		this.profile_img = profile_img;
	}

	@Override
	public String toString() {
		return "User [user_email=" + user_email + ", nickname=" + nickname + ", post_no=" + post_no + ", reg_date="
				+ reg_date + ", live=" + live + ", profile_comment=" + profile_comment + ", profile_img=" + profile_img
				+ "]";
	}
	
	
	
	
	
}
