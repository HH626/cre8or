package com.ssafysns.model.dto;

public class Profile {
	String nickname;
	String imgpath;
	String comment;
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getImgpath() {
		return imgpath;
	}

	public void setImgpath(String imgpath) {
		this.imgpath = imgpath;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Profile(String nickname, String imgpath, String comment) {
		super();
		this.nickname = nickname;
		this.imgpath = imgpath;
		this.comment = comment;
	}

	
	public Profile() {
		super();
	}

	@Override
	public String toString() {
		return "Profile [nickname=" + nickname + ", imgpath=" + imgpath + ", comment=" + comment + "]";
	}
	
	
	
}
