package com.ssafysns.model.dto;

public class LikePost {
	int like_no;
	int post_no;
	String user_email;
	
	public LikePost() {
		super();
	}

	public LikePost(int like_no, int post_no, String user_email) {
		super();
		this.like_no = like_no;
		this.post_no = post_no;
		this.user_email = user_email;
	}

	
	public int getLike_no() {
		return like_no;
	}

	public void setLike_no(int like_no) {
		this.like_no = like_no;
	}

	public int getPost_no() {
		return post_no;
	}

	public void setPost_no(int post_no) {
		this.post_no = post_no;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	@Override
	public String toString() {
		return "Follow [like_no=" + like_no + 
				", post_no=" + post_no + ", user_email=" + user_email + "]";
	}
	
	
}
