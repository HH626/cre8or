package com.ssafysns.model.dto;

public class Category {
	int ctgr_num;
	String category;

	public Category() {}

	public Category(int ctgr_num, String category) {
		super();
		this.ctgr_num = ctgr_num;
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getCtgr_num() {
		return ctgr_num;
	}

	public void setCtgr_num(int ctgr_num) {
		this.ctgr_num = ctgr_num;
	}

	@Override
	public String toString() {
		return "Category [ctgr_num=" + ctgr_num + ", category=" + category + "]";
	}

}
