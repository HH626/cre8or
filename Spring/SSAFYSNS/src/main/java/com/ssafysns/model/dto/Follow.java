package com.ssafysns.model.dto;

public class Follow {
	int follow_no;
	String from_email;
	String to_email;
	
	public Follow() {
		super();
	}

	public Follow(int follow_no, String from_email, String to_email) {
		super();
		this.follow_no = follow_no;
		this.from_email = from_email;
		this.to_email = to_email;
	}

	public int getFollow_no() {
		return follow_no;
	}

	public void setFollow_no(int follow_no) {
		this.follow_no = follow_no;
	}

	public String getFrom_email() {
		return from_email;
	}

	public void setFrom_email(String from_email) {
		this.from_email = from_email;
	}

	public String getTo_email() {
		return to_email;
	}

	public void setTo_email(String to_email) {
		this.to_email = to_email;
	}

	@Override
	public String toString() {
		return "Follow [follow_no=" + follow_no + 
				", from_email=" + from_email + ", to_email=" + to_email + "]";
	}
	
	
}
