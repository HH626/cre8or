package com.ssafysns.model.dto;

public class Post {
	int post_no;
	String title;
	String content;
	String reg_date;
	int likes;
	int keyword_no;
	String file_name;
	String file_type;
	String category;
	String user_email;
	String nickname;
	
	public Post() {
		super();
	}

	public Post(int post_no, String title, String content, String reg_date, int likes, String file_name, String file_type) {
		super();
		this.post_no = post_no;
		this.title = title;
		this.content = content;
		this.reg_date = reg_date;
		this.likes = likes;
		this.file_name = file_name;
		this.file_type = file_type;
	}

	public Post(int post_no, String title, String content, String reg_date, int likes, String file_name, int keyword_no,
			String user_email, String file_type,String nickname) {
		super();
		this.post_no = post_no;
		this.title = title;
		this.content = content;
		this.reg_date = reg_date;
		this.likes = likes;
		this.file_name = file_name;
		this.keyword_no = keyword_no;
		this.file_type = file_type;
		this.user_email = user_email;
		this.nickname = nickname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Post(int post_no, String title, String content, String reg_date, int likes, int keyword_no,
			String user_email, String file_name, String file_type, String category) {
		super();
		this.post_no = post_no;
		this.title = title;
		this.content = content;
		this.reg_date = reg_date;
		this.likes = likes;
		this.keyword_no = keyword_no;
		this.user_email = user_email;
		this.file_name = file_name;
		this.file_type = file_type;
		this.category = category;
	}

	public int getPost_no() {
		return post_no;
	}

	public void setPost_no(int post_no) {
		this.post_no = post_no;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReg_date() {
		return reg_date;
	}

	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public int getKeyword_no() {
		return keyword_no;
	}

	public void setKeyword_no(int keyword_no) {
		this.keyword_no = keyword_no;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Post [post_no=" + post_no + ", title=" + title + ", content=" + content + ", reg_date=" + reg_date
				+ ", likes=" + likes + ", keyword_no=" + keyword_no + ", user_email=" + user_email + ", file_name="
				+ file_name + ", file_type=" + file_type + ", category=" + category + "]";
	}

	

}
