package com.ssafysns.model.dao;
import org.apache.ibatis.annotations.Mapper;
import com.ssafysns.model.dto.Profile;
@Mapper
public interface ProfileDAO {
	public Profile getProfile(String nickname);

	public void insert(String nickname);

	public void updateImg(Profile profile);

	public void updateComment(Profile profile);
}