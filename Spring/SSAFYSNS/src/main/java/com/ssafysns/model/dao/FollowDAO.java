package com.ssafysns.model.dao;
import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.ssafysns.model.dto.Follow;;
@Mapper
public interface FollowDAO {
	public List<Follow> searchAll();
	public List<String> searchByfromEmail(String from_email);
	public int isFollowedByToEmail(HashMap<String,String> map);
	
	public void insert(Follow follow);
	public void delete(Follow follow);
	
}