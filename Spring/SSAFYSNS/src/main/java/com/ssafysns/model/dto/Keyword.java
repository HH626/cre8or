package com.ssafysns.model.dto;

public class Keyword {
	int keyword_no;
	String keyword;
	int section;
	String post_date;
	String end_date;
	int post_cnt;
	String img;

	public Keyword() {
		super();
	}



	public Keyword(int keyword_no, String keyword, int section, String post_date, String end_date, int post_cnt,
			String img) {
		super();
		this.keyword_no = keyword_no;
		this.keyword = keyword;
		this.section = section;
		this.post_date = post_date;
		this.end_date = end_date;
		this.post_cnt = post_cnt;
		this.img = img;
	}


	public int getKeyword_no() {
		return keyword_no;
	}

	public void setKeyword_no(int keyword_no) {
		this.keyword_no = keyword_no;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getSection() {
		return section;
	}

	public void setSection(int section) {
		this.section = section;
	}

	public String getPost_date() {
		return post_date;
	}

	public void setPost_date(String post_date) {
		this.post_date = post_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public int getPost_cnt() {
		return post_cnt;
	}

	public void setPost_cnt(int post_cnt) {
		this.post_cnt = post_cnt;
	}

	public String getImg() {
		return img;
	}


	public void setImg(String img) {
		this.img = img;
	}



	@Override
	public String toString() {
		return "Keyword [keyword_no=" + keyword_no + ", keyword=" + keyword + ", section=" + section + ", post_date="
				+ post_date + ", end_date=" + end_date + ", post_cnt=" + post_cnt + ", img=" + img + "]";
	}

}
