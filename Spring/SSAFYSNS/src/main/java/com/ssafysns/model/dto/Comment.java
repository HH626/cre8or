package com.ssafysns.model.dto;

public class Comment {
	int comment_no;
	String content;
	String reg_date;
	int post_no;
	String user_email;
	String nickname;
	
	public Comment() {
		super();
	}

	public Comment(int comment_no, String content, String reg_date, int post_no, String user_email, String nickname) {
		super();
		this.comment_no = comment_no;
		this.content = content;
		this.reg_date = reg_date;
		this.post_no = post_no;
		this.user_email = user_email;
		this.nickname = nickname;
	}

	public int getComment_no() {
		return comment_no;
	}

	public void setComment_no(int comment_no) {
		this.comment_no = comment_no;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReg_date() {
		return reg_date;
	}

	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}

	public int getPost_no() {
		return post_no;
	}

	public void setPost_no(int post_no) {
		this.post_no = post_no;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Override
	public String toString() {
		return "Comment [comment_no=" + comment_no + ", content=" + content + ", reg_date=" + reg_date + ", post_no="
				+ post_no + ", user_email=" + user_email + ", nickname=" + nickname + "]";
	}
	
}
