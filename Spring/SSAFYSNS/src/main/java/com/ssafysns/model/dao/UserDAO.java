package com.ssafysns.model.dao;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.ssafysns.model.dto.User;
@Mapper
public interface UserDAO {
	public User getUser(String email) ;
	public List<User> searchAll();
	public List<User> searchByEmail(String email);
	public List<User> searchByNickname(String nickname);
	public List<User> searchByLive(String live);
	public void insert(User user);
	public void update(User user);
	public void updateLive(User user);
	
	public List<User> searchByfromEmail(String from_email);
	
	
}