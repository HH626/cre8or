package com.ssafysns.model.dao;
import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.ssafysns.model.dto.LikePost;
import com.ssafysns.model.dto.Post;
@Mapper
public interface LikePostDAO {
	public List<LikePost> searchAll();
	//나를 좋아요 한 포스트
	public List<Post> searchByEmail(String user_email);
	//내가 좋아요 한 포스트 번호
	public List<Integer> searchPostNum(String user_email);
	//포스트의 좋아요 숫자
	public int searchLikeCnt(int post_no);
	public void insert(LikePost likepost);
	public void delete(LikePost likepost);
	public int isLikePostByToEmail(HashMap<String, Object> map);
	
}