package com.ssafysns.model.dto;

public class postKeywordNCategory {
	int keyword_no;
	String category;
	
	public postKeywordNCategory(int keyword_no, String category) {
		super();
		this.keyword_no = keyword_no;
		this.category = category;
	}
	public int getKeyword_no() {
		return keyword_no;
	}
	public void setKeyword_no(int keyword_no) {
		this.keyword_no = keyword_no;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "postKeywordNCategory [keyword_no=" + keyword_no + ", category=" + category + "]";
	}
	
	
}
