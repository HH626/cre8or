package com.ssafysns.model.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafysns.model.dto.Category;

@Mapper
public interface CategoryDAO {
	public List<Category> searchAll();
	public Category search(String category);
	public List<Category> searchList(String category);
	public void insert(Category category);
	public void delete(String category);
}