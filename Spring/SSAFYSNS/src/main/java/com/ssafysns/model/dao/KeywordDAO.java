package com.ssafysns.model.dao;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ssafysns.model.dto.Keyword;
@Mapper
public interface KeywordDAO {
	
	public List<Keyword> searchAll();
	public List<Keyword> searchByKeyword_no(int keyword_no);
	/**단어로 키워드 검색*/
	public List<Keyword> searchByKeyword(String keyword);
	/**일간검색어들, 주간검색어들, 월간검색어들 검색*/
	public List<Keyword> searchBySection(int section);
	/**해당 날짜 키워드 검색 (일,주,월간)*/
	public List<Keyword> searchByDate(String date);	
	public void insert(Keyword keyword);
	public void update(Keyword keyword);
	public void delete(int keyword_no);
}