package com.ssafysns.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ssafysns.model.dto.User;
import com.ssafysns.service.UserService;

import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins = "*", maxAge = 6000)
@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@ApiOperation(value = "Email로 해당 User 조회")
	@GetMapping("/user/getUser/{email}")
	public ResponseEntity<Map<String, Object>> getUser(@PathVariable String email) throws Exception {

		System.out.println("getuser>>>>"+userService.getUser(email));
		return handleSuccess(userService.getUser(email));
	}
	
	@PostMapping("/user/addUser")
	public ResponseEntity<Map<String, Object>> addUser(@RequestBody User user) throws Exception {
		userService.insert(user);
		return handleSuccess("");
	}
	
	@PutMapping("/user/update")
	public ResponseEntity<Map<String, Object>> update(@RequestBody User user) throws Exception{
		System.out.println("update>>>>>>>>>>>>>");
		userService.update(user);
		return handleSuccess("");
	}
	@PutMapping("/user/updateLive")
	public ResponseEntity<Map<String, Object>> updateLive(@RequestBody User user) throws Exception{
		userService.updateLive(user);
		return handleSuccess("");
	}
	/********************관리자********************************/
	@ApiOperation(value = "모든 User 목록 조회")
	@GetMapping("/user/searchAll")
	public ResponseEntity<Map<String, Object>> SearchAll() throws Exception {
		return handleSuccess(userService.searchAll());
	}
	
	@ApiOperation(value = "Email로 User 목록 조회")
	@GetMapping("/user/searchByEmail")
	public ResponseEntity<Map<String, Object>> searchByEmail(@RequestParam String email) throws Exception {
		return handleSuccess(userService.searchByEmail(email));
	}
	@ApiOperation(value = "닉네임으로 User 목록 조회")
	@GetMapping("/user/searchByNickname")
	public ResponseEntity<Map<String, Object>> searchByNickname(@RequestParam String nickname) throws Exception {
		return handleSuccess(userService.searchByNickname(nickname));
	}
	@ApiOperation(value = "탈퇴여부로 User 목록 조회")
	@GetMapping("/user/searchByLive")
	public ResponseEntity<Map<String, Object>> searchByLive(@RequestParam String live) throws Exception {
		return handleSuccess(userService.searchByLive(live));
	}
	
	@ApiOperation(value = "내 Email로 내가 즐겨찾기한 작가 이메일 목록 조회")
	@GetMapping("/mypage/user/searchFollowByFromEmail/{from_email}")
	public ResponseEntity<Map<String, Object>> searchByfromEmail(@PathVariable String from_email) throws Exception {
		return handleSuccess(userService.searchByfromEmail(from_email));
	}

	
	

	public ResponseEntity<Map<String, Object>> handleFail(Object data, HttpStatus state) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "fail");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, state);
	}

	public ResponseEntity<Map<String, Object>> handleSuccess(Object data) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "ok");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}