package com.ssafysns.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ssafysns.model.dto.LikePost;
import com.ssafysns.model.dto.Post;
import com.ssafysns.service.LikePostService;
import com.ssafysns.service.PostService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*" , maxAge = 6000)
@RestController
public class PostController {
	@Autowired
	private PostService postService;
	@Autowired
	private LikePostService likepostservice;

	@ApiOperation(value = "모든 Post 목록 조회")
	@GetMapping("/post/searchAll")
	public ResponseEntity<Map<String, Object>> searchAll() throws Exception {
		System.out.println("post searchAll >>>>>>>>");
		return handleSuccess(postService.searchAll());
	}

	@ApiOperation(value = "Post 10개씩 조회")
	@GetMapping("/post/searchLimit/{limit}")
	public ResponseEntity<Map<String, Object>> searchLimit(@PathVariable int limit) throws Exception {
		System.out.println("post Limit>>>>> ");
		return handleSuccess(postService.searchLimit(limit));
	}
	
	@ApiOperation(value = "일/주/월간 키워드 별 게시글 조회")
	@GetMapping("/post/searchSection/{section}")
	public ResponseEntity<Map<String, Object>> searchSection(@PathVariable int section) throws Exception {
		System.out.println("post searchSection >>>>>>>>" + "section::" + section);
		System.out.println("post searchSection >>>>>>>>" + postService.searchSection(section));
		return handleSuccess(postService.searchSection(section));
	}
	
	@ApiOperation(value = "일/주/월간 키워드 별 게시글 10개씩 조회")
	@GetMapping("/post/searchSectionLimit/{section}/{limit}")
	public ResponseEntity<Map<String, Object>> searchSectionLimit(@PathVariable int section, @PathVariable int limit) throws Exception {
		System.out.println("post searchSectionLimit >>>>>>>>" + "section::" + section + " limit::" + limit);
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("section", section);
		map.put("limit", limit);
		System.out.println("post searchSectionLimit >>>>>>>>" + postService.searchSectionLimit(map));
		return handleSuccess(postService.searchSectionLimit(map));
	}
	
	@ApiOperation(value = "해당 날짜의 일간 랭킹 50개 조회")
	@GetMapping("/post/searchSectionLimit/{date}")
	public ResponseEntity<Map<String, Object>> searchRankDaily(@PathVariable String date ) throws Exception {
		return handleSuccess(postService.searchRankDaily(date));
	}
	
	
	@ApiOperation(value = " Post의 좋아요  조회")
	@GetMapping("/post/seachPostCnt")
	public ResponseEntity<Map<String, Object>> seachPostCnt(@RequestParam int post_no) throws Exception {
		return handleSuccess(likepostservice.searchLikeCnt(post_no));
	}


	@GetMapping("/post/searchPostno/{post_no}")
	public ResponseEntity<Map<String, Object>> searchPostno(@PathVariable int post_no) throws Exception {
		System.out.println("post searchSection >>>>>>>>" + post_no);
		return handleSuccess(postService.searchPostno(post_no));
	}

	@ApiOperation(value = "키워드별  Post 목록 조회")
	@GetMapping("/post/searchbyKeyword/{keyword_no}")
	public ResponseEntity<Map<String, Object>> searchbyKeyword(@PathVariable int keyword_no) throws Exception {
		return handleSuccess(postService.searchbyKeyword(keyword_no));
	}
	
	@GetMapping("/post/searchKeywordandCategory/{keyword_no}/{category}")
	public ResponseEntity<Map<String, Object>> searchKeywordandCategory(@PathVariable int keyword_no, @PathVariable String category) throws Exception {
		System.out.println("searchKeywordandCategory/{keyword_no}_{category}>>>>>>>"+ keyword_no+" " +category);
		System.out.println("OKOKOKOK");
		
		return handleSuccess(postService.searchKeywordandCategory(keyword_no,category));
	}

	@ApiOperation(value = "해당 사용자의 Post 목록 조회")
	@GetMapping("/post/searchByEmail")
	public ResponseEntity<Map<String, Object>> searchByEmail(@RequestParam String user_email) throws Exception {
		return handleSuccess(postService.searchByEmail(user_email));
	}

	@PostMapping("/post/addPost")
	public ResponseEntity<Map<String, Object>> addPost(@RequestBody Post post) throws Exception {
		System.out.println("추가될 post정보 >>>>>>>>" + post);
		return handleSuccess(postService.insert(post));
	}

	@PutMapping("/post/update")
	public ResponseEntity<Map<String, Object>> updatePost(@RequestBody Post post) throws Exception {
		return handleSuccess(postService.update(post));
	}
	
	@DeleteMapping("/post/delete/{post_no}")
	public ResponseEntity<Map<String, Object>> deletePost(@PathVariable int post_no) throws Exception {
		return handleSuccess(postService.delete(post_no));
	}
	
	@ApiOperation(value = "좋아요 개수추가")
	@PutMapping("/post/addLikePostCnt/{post_no}")
	public ResponseEntity<Map<String, Object>> addLikePostCnt(@PathVariable int post_no) throws Exception {
		postService.addLike(post_no);
		return handleSuccess("");
	}
	
	@ApiOperation(value = "좋아요 개수감소")
	@PutMapping("/post/subLikePostCnt/{post_no}")
	public ResponseEntity<Map<String, Object>> subLikePostCnt(@PathVariable int post_no) throws Exception {
		postService.subLike(post_no);
		return handleSuccess("");
	}

	public ResponseEntity<Map<String, Object>> handleFail(Object data, HttpStatus state) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "fail");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, state);
	}

	public ResponseEntity<Map<String, Object>> handleSuccess(Object data) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "ok");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}