package com.ssafysns.controller;

import java.util.HashMap;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ssafysns.model.dto.Keyword;
import com.ssafysns.model.dto.Post;
import com.ssafysns.service.KeywordService;
import com.ssafysns.service.PostService;

import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins ="*", maxAge = 6000)
@RestController
public class KeywordController {
	@Autowired
	private KeywordService keywordService;

	@ApiOperation(value = "모든 keyword 목록 조회")
	@GetMapping("/keyword/searchAll")
	public ResponseEntity<Map<String, Object>> SearchAll() throws Exception {
		System.out.println("keyword/searchAll>>>>");
		return handleSuccess(keywordService.searchAll());
	}
	@ApiOperation(value = "키워드번호로 keyword 목록 조회")
	@GetMapping("/keyword/searchByKeyword_no/{keyword_no}")
	public ResponseEntity<Map<String, Object>> searchByKeyword_no(@PathVariable int keyword_no) throws Exception {
		return handleSuccess(keywordService.searchByKeyword_no(keyword_no));
	}
	@ApiOperation(value = "키워드로 keyword 목록 조회")
	@GetMapping("/keyword/searchByKeyword/{keyword}")
	public ResponseEntity<Map<String, Object>> searchByKeyword(@PathVariable String keyword) throws Exception {
		return handleSuccess(keywordService.searchByKeyword(keyword));
	}
	@ApiOperation(value = "기간별 keyword 목록 조회")
	@GetMapping("/keyword/searchBySection/{section}")
	public ResponseEntity<Map<String, Object>> searchBySection(@PathVariable int section) throws Exception {
		return handleSuccess(keywordService.searchBySection(section));
	}
	@ApiOperation(value = "특정 날짜 keyword 조회")
	@GetMapping("/keyword/searchByTheDate/{date}")
	public ResponseEntity<Map<String, Object>> searchByTheDate(@PathVariable String date) throws Exception {
		System.out.println("search by date >>>>>>>");
		return handleSuccess(keywordService.searchByTheDate(date));
	}
	
	@ApiOperation(value = "해당 날짜 keyword 조회")
	@GetMapping("/keyword/searchByDate")
	public ResponseEntity<Map<String, Object>> searchByDate(String date) throws Exception {
		System.out.println("search by date >>>>>>>");
		return handleSuccess(keywordService.searchByDate(date));
	}
	
	
	/*******************관리자 영역**********************/
	@PostMapping("/keyword/insert")
	public ResponseEntity<Map<String, Object>> insert(@RequestBody Keyword keyword) throws Exception {
		System.out.println("insert");
		System.out.println(keyword.toString());
		keywordService.insert(keyword);
		return handleSuccess("");
	}
	@PutMapping("/keyword/update")
	public ResponseEntity<Map<String, Object>> update(@RequestBody Keyword keyword) throws Exception {
		keywordService.update(keyword);
		return handleSuccess("");
	}
	
	@DeleteMapping("/keyword/delete")
	public ResponseEntity<Map<String, Object>> delete(@RequestParam String keyword_no) throws Exception{
		System.out.println("del");
		int no=Integer.parseInt(keyword_no);
		keywordService.delete(no);
		return handleSuccess("");
	}

	
	
	public ResponseEntity<Map<String, Object>> handleFail(Object data, HttpStatus state) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "fail");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, state);
	}

	public ResponseEntity<Map<String, Object>> handleSuccess(Object data) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "ok");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}