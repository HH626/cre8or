package com.ssafysns.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ssafysns.model.dto.Follow;
import com.ssafysns.model.dto.LikePost;
import com.ssafysns.model.dto.Profile;
import com.ssafysns.model.dto.User;
import com.ssafysns.service.FollowService;
import com.ssafysns.service.LikePostService;
import com.ssafysns.service.PostService;
import com.ssafysns.service.ProfileService;

import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins = "*", maxAge = 6000)
@RestController
public class MypageController {
	@Autowired
	private FollowService followService;
	@Autowired
	private PostService postService;
	@Autowired
	private LikePostService likepostService;
	@Autowired
	private ProfileService profileService;

	/*내 프로필 수정*/
	@ApiOperation(value = "내 프로필 조회")
	@GetMapping("/mypage/profile/searchProfile")
	public ResponseEntity<Map<String, Object>> searchprofile(@RequestParam String nickname)throws Exception{
		return handleSuccess(profileService.getProfile(nickname));
	}
	@ApiOperation(value = "프로필 생성")
	@PostMapping("/mypage/profile/addProfile")
	public ResponseEntity<Map<String, Object>> addProfile(@RequestParam  String nickname) throws Exception {
		profileService.insert(nickname);
		return handleSuccess("");
	}
	@ApiOperation(value = "이미지 수정")
	@PutMapping("/mypage/post/updateimgpath")
	public ResponseEntity<Map<String, Object>> updateimgpath(@RequestBody Profile profile) throws Exception{
		System.out.println("update>>>>>>>>>>>>>");
		profileService.updateImg(profile);
		return handleSuccess("");
	}
	@ApiOperation(value = "한줄 소개 수정")
	@PutMapping("/mypage/post/updatecomment")
	public ResponseEntity<Map<String, Object>> updatecomment(@RequestBody Profile profile) throws Exception{
		System.out.println("update>>>>>>>>>>>>>");
		profileService.updateComment(profile);
		return handleSuccess("");
	}
	/*내가 쓴 글 보기*/
	@ApiOperation(value = "내가 쓴 글 목록 조회")
	@GetMapping("/mypage/post/searchByEmail")
	public ResponseEntity<Map<String, Object>> searchPostByEmail(@RequestParam String user_email)throws Exception{
		return handleSuccess(postService.searchByEmail(user_email));
	}

	
	/*즐겨찾기 */
	/********************관리자********************************/
	@ApiOperation(value = "모든 즐겨찾기 목록 조회")
	@GetMapping("/mypage/follow/searchAllFollow")
	public ResponseEntity<Map<String, Object>> SearchAllFollow() throws Exception {
		return handleSuccess(followService.searchAll());
	}
	
	@ApiOperation(value = "내 Email로 내가 즐겨찾기한 작가 이메일 목록 조회")
	@GetMapping("/mypage/follow/searchFollowByFromEmail/{from_email}")
	public ResponseEntity<Map<String, Object>> searchFollowByFromEmail(@PathVariable String from_email) throws Exception {
		System.out.println("searchFollowByFromEmail");
		return handleSuccess(followService.searchByfromEmail(from_email));
	}
	
	@ApiOperation(value = "즐겨찾기 한 유저인지 확인")
	@GetMapping("/mypage/follow/checkFollowUser/{from_email}/{to_email}")
	public ResponseEntity<Map<String, Object>> checkFollowUser(@PathVariable String from_email, @PathVariable String to_email) throws Exception {
		Follow follow = new Follow();
		follow.setFrom_email(from_email);
		follow.setTo_email(to_email);
		//followService.check(follow);
		return handleSuccess(followService.check(follow));
	}
	@ApiOperation(value = "내 Email로 즐겨찾기 isfollowed 판별")
	@GetMapping("/mypage/likepost/isFollowedByToEmail/{from_email}/{to_email}")
	public ResponseEntity<Map<String, Object>> isFollowedByToEmail(@PathVariable String from_email, @PathVariable String to_email) throws Exception {
		System.out.println("search like by email"+from_email+""+"포스트 번호"+from_email);
		HashMap<String, String> map=new HashMap<String, String>();
		map.put("from_email", from_email);
		map.put("to_email", to_email);
		
		return handleSuccess(followService.isFollowedByToEmail(map));
	}
	@ApiOperation(value = "즐겨찾기 추가")
	@PostMapping("/mypage/follow/addFollowUser")
	public ResponseEntity<Map<String, Object>> addFollowUser(@RequestBody Follow follow) throws Exception {
		System.out.println("add follow user");
		followService.insert(follow);
		return handleSuccess("");
	}
	
	@ApiOperation(value = "즐겨찾기 삭제")
	@DeleteMapping("/mypage/follow/deleteFollow")
	public ResponseEntity<Map<String, Object>> deleteFollow(@RequestBody Follow follow) throws Exception{
		followService.delete(follow);
		return handleSuccess("");
	}
	
	/*좋아요 한 post */
	/********************관리자********************************/
	@ApiOperation(value = "모든 즐겨찾기 목록 조회")
	@GetMapping("/mypage/likepost/searchAllPost")
	public ResponseEntity<Map<String, Object>> SearchAllPost() throws Exception {
		return handleSuccess(likepostService.searchAll());
	}
	
	@ApiOperation(value = "내 Email로 내가 좋아요 한 포스트 조회")
	@GetMapping("/mypage/likepost/searchLikePostByToEmail")
	public ResponseEntity<Map<String, Object>> searchLikePostByToEmail(@RequestParam String user_email) throws Exception {
		return handleSuccess(likepostService.searchByEmail(user_email));
	}
	
	@ApiOperation(value = "내 Email로 내가 좋아요 한 포스트 넘버 조회")
	@GetMapping("/mypage/likepost/searchLikePostNumByToEmail/{user_email}")
	public ResponseEntity<Map<String, Object>> searchLikePostNumByToEmail(@PathVariable String user_email) throws Exception {
		System.out.println("search like by email"+user_email);
		return handleSuccess(likepostService.searchPostNum(user_email));
	}
	
	@ApiOperation(value = "내 Email로 포스트 넘버로 isliked 판별")
	@GetMapping("/mypage/likepost/isLikePostByToEmail/{user_email}/{post_no}")
	public ResponseEntity<Map<String, Object>> isLikePostByToEmail(@PathVariable String user_email, @PathVariable int post_no) throws Exception {
		System.out.println("search like by email"+user_email+""+"포스트 번호"+post_no);
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("post_no", post_no);
		map.put("user_email", user_email);
		
		return handleSuccess(likepostService.isLikePostByToEmail(map));
	}
	
	@ApiOperation(value = "좋아요 추가")
	@PostMapping("/mypage/likepost/addLikePostUser")
	public ResponseEntity<Map<String, Object>> addLikePostUser(@RequestBody LikePost likepost) throws Exception {
		likepostService.insert(likepost);
		return handleSuccess("");
	}
	

	
	@ApiOperation(value = "좋아요 삭제")
	@DeleteMapping("/mypage/likepost/deleteLikePost")
	public ResponseEntity<Map<String, Object>>  deleteLikePost(@RequestBody LikePost likepost) throws Exception{
		likepostService.delete(likepost);
		return handleSuccess("");
	}
	
	public ResponseEntity<Map<String, Object>> handleFail(Object data, HttpStatus state) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "fail");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, state);
	}

	public ResponseEntity<Map<String, Object>> handleSuccess(Object data) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "ok");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}