package com.ssafysns.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ssafysns.model.dto.Category;
import com.ssafysns.service.CategoryService;

import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins ="*", maxAge = 6000)
@RestController
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@ApiOperation(value = "모든 카테고리 조회")
	@GetMapping("/category/searchAll")
	public ResponseEntity<Map<String, Object>> searchAll() throws Exception {
		return handleSuccess(categoryService.searchAll());
	}
	
	@ApiOperation(value = "카테고리 하나 조회")
	@GetMapping("/category/search/{category}")
	public ResponseEntity<Map<String, Object>> search(@PathVariable String category) throws Exception {
		return handleSuccess(categoryService.search(category));
	}
	
	@ApiOperation(value = "카테고리 포함 목록 조회")
	@GetMapping("/category/searchList/{category}")
	public ResponseEntity<Map<String, Object>> searchList(@PathVariable String category) throws Exception {
		return handleSuccess(categoryService.searchList(category));
	}
	
	@ApiOperation(value = "카테고리 등록")
	@PostMapping("/category/insert")
	public ResponseEntity<Map<String, Object>> update(@RequestBody Category category) throws Exception {
		categoryService.insert(category);
		return handleSuccess("");
	}
	
	@ApiOperation(value = "카테고리 삭제")
	@DeleteMapping("/category/delete")
	public ResponseEntity<Map<String, Object>> delete(@RequestBody String category) throws Exception{
		categoryService.delete(category);
		return handleSuccess("");
	}

	
	
	public ResponseEntity<Map<String, Object>> handleFail(Object data, HttpStatus state) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "fail");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, state);
	}

	public ResponseEntity<Map<String, Object>> handleSuccess(Object data) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "ok");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}