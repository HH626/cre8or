package com.ssafysns.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ssafysns.model.dto.Comment;
import com.ssafysns.service.CommentService;

import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins ="*", maxAge = 6000)
@RestController
public class CommentController {
	@Autowired
	private CommentService commentService;

	@ApiOperation(value = "해당 게시글의 모든 댓글 조회")
	@GetMapping("/comment/searchAll/{post_no}")
	public ResponseEntity<Map<String, Object>> searchByPostno(@PathVariable int post_no) throws Exception {
		System.out.println(post_no);
		System.out.println(commentService.searchByPostno(post_no));
		return handleSuccess(commentService.searchByPostno(post_no));
	}
	
	@PostMapping("/comment/insert")
	public ResponseEntity<Map<String, Object>> insert(@RequestBody Comment comment) throws Exception {
		commentService.insert(comment);
		return handleSuccess("");
	}
	@PutMapping("/comment/update")
	public ResponseEntity<Map<String, Object>> update(@RequestBody Comment comment) throws Exception {
		commentService.update(comment);
		System.out.println(comment);
		return handleSuccess("");
	}
	
	@DeleteMapping("/comment/delete/{comment_no}")
	public ResponseEntity<Map<String, Object>> delete(@PathVariable int comment_no) throws Exception{
		commentService.delete(comment_no);
		return handleSuccess("");
	}

	
	
	public ResponseEntity<Map<String, Object>> handleFail(Object data, HttpStatus state) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "fail");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, state);
	}

	public ResponseEntity<Map<String, Object>> handleSuccess(Object data) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("state", "ok");
		resultMap.put("data", data);
		return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK);
	}
}