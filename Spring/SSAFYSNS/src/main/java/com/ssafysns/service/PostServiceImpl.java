package com.ssafysns.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.KeywordDAO;
import com.ssafysns.model.dao.PostDAO;
import com.ssafysns.model.dto.Post;
//import com.ssafysns.model.dto.UserException;
import com.ssafysns.model.dto.postKeywordNCategory;

@Service
public class PostServiceImpl implements PostService {
	@Autowired
	private PostDAO dao;
	
	public List<Post> searchAll() {
		return dao.searchAll();
	}
	
	public List<Post> searchLimit(int limit) {
		return dao.searchLimit(limit);
	}
	
	public List<Post> searchRankDaily(String date) {
		return dao.searchRankDaily(date);
	}
	
	public List<Post> searchSection(int section) {
		return dao.searchSection(section);
	}
	
	public List<Post> searchSectionLimit(HashMap<String, Integer> map) {
		return dao.searchSectionLimit(map);
	}

	public Post searchPostno(int post_no) {
		return dao.searchPostno(post_no);
	}
	
	public List<Post> searchbyKeyword(int keyword_no) {
		return dao.searchbyKeyword(keyword_no);
	}

	public int searchKeywordandCategory(int keyword_no, String category){
		postKeywordNCategory KNC = new postKeywordNCategory(keyword_no, category);
		return dao.searchKeywordandCategory(KNC);
	}
	public List<Post> searchByEmail(String user_email) {
		return dao.searchByEmail(user_email);
	}
	public String addLike(int post_no) {
	 dao.addLike(post_no);	
	 return "ok";
	}
	public String subLike(int post_no) {
		dao.subLike(post_no);
		return "ok";
	}

	public String insert(Post post) {
		SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss");
		Date time = new Date();
		String time1 = format1.format(time);
		post.setReg_date(time1);
		dao.insert(post);
		return "ok";
	}

	public String update(Post post) {
		dao.update(post);
		return "ok";
	}

	public String delete(int post_no) {
		dao.delete(post_no);
		return "ok";
	}






}