package com.ssafysns.service;

import java.util.HashMap;
import java.util.List;
import com.ssafysns.model.dto.LikePost;
import com.ssafysns.model.dto.Post;

public interface LikePostService {
	public List<LikePost> searchAll();
	//내가 좋아요 한 포스트
	public List<Post> searchByEmail(String user_email);
	//내가 좋아요 한 포스트 넘버 
	public List<Integer> searchPostNum(String user_email);
	public int searchLikeCnt(int post_no);
	public void insert(LikePost likepost);
	public void delete(LikePost likepost);
	public int isLikePostByToEmail(HashMap<String, Object> map);
	
}
