package com.ssafysns.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.FollowDAO;
import com.ssafysns.model.dto.Follow;

@Service
public class FollowServiceImpl implements FollowService {
	@Autowired
	private FollowDAO dao;

	@Override
	public List<Follow> searchAll() {
		// TODO Auto-generated method stub
		return dao.searchAll();
	}

	@Override
	public List<String> searchByfromEmail(String from_email) {
		// TODO Auto-generated method stub
		return dao.searchByfromEmail(from_email);
	}
	
	public int isFollowedByToEmail(HashMap<String, String> map) {
		return dao.isFollowedByToEmail(map);
	}

	
	@Override
	public boolean check(Follow follow){
		List<String> list = dao.searchByfromEmail(follow.getFrom_email());
		for (String str : list) {
			System.out.println(str);
			if(str.equals(follow.getTo_email())) return true;
		}
		return false;
	}

	@Override
	public void insert(Follow follow) {
		System.out.println("추가될 post정보 >>>>>>>>"+follow);
		dao.insert(follow);
	}

	@Override
	public void delete(Follow follow) {
		dao.delete(follow);
	}

}
