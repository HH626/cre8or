package com.ssafysns.service;


import com.ssafysns.model.dto.Profile;

public interface ProfileService {
	public Profile getProfile(String nickname);

	public void insert(String nickname);

	public void updateImg(Profile profile);

	public void updateComment(Profile profile);
}
