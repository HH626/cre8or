package com.ssafysns.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.ProfileDAO;
import com.ssafysns.model.dto.Profile;
@Service
public class ProfileServiceImpl implements ProfileService {
	@Autowired
	private ProfileDAO dao;
	@Override
	public Profile getProfile(String nickname) {
		Profile profile=dao.getProfile(nickname);
		return profile;
	}

	@Override
	public void insert(String nickname) {
		dao.insert(nickname);
	}

	@Override
	public void updateImg(Profile profile) {
		//사용자가 이미지를 기본으로 하면 defualt라는 값이 들어온다.
		if (profile.getImgpath().equals("defualt")) {
			profile.setImgpath("./defualt.jpg");
		}
		System.out.println(profile);
		dao.updateImg(profile);
	}

	@Override
	public void updateComment(Profile profile) {
		System.out.println(profile);
		dao.updateComment(profile);
	}

}
