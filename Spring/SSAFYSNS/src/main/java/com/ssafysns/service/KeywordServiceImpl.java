package com.ssafysns.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.KeywordDAO;
import com.ssafysns.model.dto.Keyword;
//import com.ssafysns.model.dto.UserException;

@Service
public class KeywordServiceImpl implements KeywordService {
	@Autowired
	private KeywordDAO dao;

	public List<Keyword> searchAll() {
		return dao.searchAll();
	}

	public List<Keyword> searchByKeyword_no(int keyword_no) {
		return dao.searchByKeyword_no(keyword_no);
	}
	public List<Keyword> searchByKeyword(String keyword){
		return dao.searchByKeyword(keyword);
	}
	public List<Keyword> searchBySection(int section){
		return dao.searchBySection(section);
	}
	public List<Keyword> searchByDate(String date) {
		SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd");
		Date d = new Date();	//지금 날짜
		date = format1.format(d);
		return dao.searchByDate(date);
	}
	public List<Keyword> searchByTheDate(String date) {
//		Date d = new Date();	//지금 날짜
//		date = format1.format(d);
//		System.out.println(date);
		return dao.searchByDate(date);
	}
	
	public void insert(Keyword keyword) {
		/** D: 일간 키워드
		 *  W: 주간 키워드
		 *  M: 월간키워드
		 */
		dao.insert(keyword);
//		try {
//			SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd");
//			Date date = new Date();
//			String time1 = format1.format(date);
//			keyword.setPost_date(time1);
//			dao.insert(keyword);
//			return "ok";
//			dao.insert(keyword);
//			return "ok"
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new Exception();
//		}
		
	}
	public void update(Keyword keyword) {
		dao.update(keyword);
	}
	public void delete(int keyword_no) {
		dao.delete(keyword_no);
	}



}