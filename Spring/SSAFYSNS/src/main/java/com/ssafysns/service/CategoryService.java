package com.ssafysns.service;

import java.util.List;

import com.ssafysns.model.dto.Category;

public interface CategoryService {
	public List<Category> searchAll();
	public Category search(String category);
	public List<Category> searchList(String category);
	public void insert(Category category);
	public void delete(String category);
}
