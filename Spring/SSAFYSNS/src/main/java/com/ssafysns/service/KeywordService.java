package com.ssafysns.service;

import java.util.List;

import com.ssafysns.model.dto.Keyword;

public interface KeywordService {
		
	public List<Keyword> searchAll();
	public List<Keyword> searchByKeyword_no(int keyword_no);
	public List<Keyword> searchByKeyword(String keyword);
	/** section : D(일간) W(주간) M(월간) */
	public List<Keyword> searchBySection(int section); 
	/** date형식 : yyyy-mm-dd*/
	public List<Keyword> searchByDate(String date);	
	public List<Keyword> searchByTheDate(String date);
	public void insert(Keyword keyword);
	public void update(Keyword keyword);
	public void delete(int keyword_no);
	
}