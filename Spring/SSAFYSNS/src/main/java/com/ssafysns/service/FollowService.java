package com.ssafysns.service;

import java.util.HashMap;
import java.util.List;
import com.ssafysns.model.dto.Follow;

public interface FollowService {
	public List<Follow> searchAll();
	//내가 즐겨찾기한
	public List<String> searchByfromEmail(String from_email);
	public int isFollowedByToEmail(HashMap<String, String> map);
	public boolean check(Follow follow);
	public void insert(Follow follow);
	public void delete(Follow follow);
	
}
