package com.ssafysns.service;

import java.util.HashMap;
import java.util.List;

import com.ssafysns.model.dto.Post;

public interface PostService {
	public List<Post> searchAll();
	public List<Post> searchLimit(int limit);
	public List<Post> searchRankDaily(String date);
	public List<Post> searchbyKeyword(int keyword_no);
	public List<Post> searchByEmail(String user_email);
	public List<Post> searchSection(int section);
	public List<Post> searchSectionLimit(HashMap<String, Integer> map);
	public int searchKeywordandCategory(int keyword_no, String category);
	public Post searchPostno(int post_no);
	public String addLike(int post_no);
	public String subLike(int post_no);
	public String insert(Post post);
	public String update(Post post);
	public String delete(int post_no);
}