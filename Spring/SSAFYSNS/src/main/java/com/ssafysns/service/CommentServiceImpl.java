package com.ssafysns.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.CommentDAO;
import com.ssafysns.model.dto.Comment;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	private CommentDAO dao;
	public List<Comment> searchByPostno(int post_no){
		return dao.searchByPostno(post_no);
	}
	
	public void insert(Comment comment) {
		SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss");
		Date time = new Date();
		String time1 = format1.format(time);
		comment.setReg_date(time1);
		dao.insert(comment);
	}
	public void update(Comment comment) {
		dao.update(comment);
	}
	public void delete(int comment_no) {
		dao.delete(comment_no);
	}
	
	


}