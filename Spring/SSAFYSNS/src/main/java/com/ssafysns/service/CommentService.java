package com.ssafysns.service;


import java.util.List;

import com.ssafysns.model.dto.Comment;

public interface CommentService {
	public List<Comment> searchByPostno(int post_no);
	public void insert(Comment comment);
	public void update(Comment comment);
	public void delete(int comment_no);
}
