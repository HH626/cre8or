package com.ssafysns.service;

import java.util.List;

import com.ssafysns.model.dto.User;

public interface UserService {
	public User getUser(String email) throws Exception;
	
	public List<User> searchAll();
	public List<User> searchByEmail(String email);
	public List<User> searchByNickname(String nickname);
	public List<User> searchByLive(String live);
	public void insert(User user);
	public void update(User user);
	public void updateLive(User user);
	
	public List<User> searchByfromEmail(String from_email);
}
