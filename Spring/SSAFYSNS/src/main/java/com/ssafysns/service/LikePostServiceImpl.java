package com.ssafysns.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.LikePostDAO;
import com.ssafysns.model.dto.LikePost;
import com.ssafysns.model.dto.Post;

@Service
public class LikePostServiceImpl implements LikePostService {
	@Autowired
	private LikePostDAO dao;
	
	@Override
	public List<LikePost> searchAll() {
		// TODO Auto-generated method stub
		return dao.searchAll();
	}

	@Override
	public List<Post> searchByEmail(String user_email) {

		return dao.searchByEmail(user_email);
	}
	@Override
	public List<Integer> searchPostNum(String user_email) {
		
		return dao.searchPostNum(user_email);
	}
	@Override
	public int searchLikeCnt(int post_no) {
		
		return dao.searchLikeCnt(post_no);
	}
	@Override
	public int isLikePostByToEmail(HashMap<String, Object> map) {
		return dao.isLikePostByToEmail(map);
	}
	@Override
	public void insert(LikePost likepost) {
		System.out.println("추가될 좋아요정보 >>>>>>>>"+likepost);
		dao.insert(likepost);
	}

	@Override
	public void delete(LikePost likepost) {
		dao.delete(likepost);
	}




}
