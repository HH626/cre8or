package com.ssafysns.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafysns.model.dao.CategoryDAO;
import com.ssafysns.model.dto.Category;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryDAO dao;
	public List<Category> searchAll() {
		System.out.println(dao.searchAll());
		return dao.searchAll();
	}

	public Category search(String category) {
		return dao.search(category);
	}

	public List<Category> searchList(String category) {
		return dao.searchList(category);
	}

	public void insert(Category category) {
		try {
			dao.insert(category);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void delete(String category) {
		dao.delete(category);
	}

}
