package com.ssafysns.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ssafysns.model.dao.UserDAO;
import com.ssafysns.model.dto.User;
//import com.ssafysns.model.dto.UserException;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO dao;
	
	String defaultImg = "https://firebasestorage.googleapis.com/v0/b/cre8or-2a293.appspot.com/o/default.jpg?alt=media&token=e9ba40be-cc3a-469c-9742-744ff2555efe";
	public User getUser(String email) throws Exception {
		/** Y: 가입 
		 *  N: 탈퇴
		 *  AD: 관리자
		 */
		User user = dao.getUser(email);
		return user;
//		try {
//			User user = dao.getUser(email);
//			if(user==null) {
//				throw new Exception( "요청하신 회원이 존재하지 않습니다");
//			}
//			if(user.getLive().equals("N")) {
//				throw new Exception( "탈퇴한 회원입니다");
//			}else {
//				return user;
//			} 
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new Exception();
//		}
	}

	public List<User> searchAll() {
		return dao.searchAll();
	}
	
	public List<User> searchByEmail(String email){
		return dao.searchByEmail(email);
	}
	public List<User> searchByNickname(String nickname){
		return dao.searchByNickname(nickname);
	}
	public List<User> searchByLive(String live){
		return dao.searchByLive(live);
	}
	public void insert(User user) {
		/**user(user_email; nickname; post_no; reg_date; live;)
		 * live는 DB에서 디폴트 값 'Y'
		 */
		SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss");
		Date time = new Date();
		String time1 = format1.format(time);
		user.setReg_date(time1);
		System.out.println(defaultImg);
		System.out.println("userservice insert>>>>>>"+ user.toString());
		user.setProfile_img(defaultImg);
		System.out.println("userservice insert>>>>>>"+ user.toString());
		dao.insert(user);                                                                                                                                                                                                                                                                                                                                                          
	}
	public void update(User user) {
		if(user.getProfile_img()==null) {
			user.setProfile_img(defaultImg);
		}
		dao.update(user);
	}
	public void updateLive(User user) {
		dao.updateLive(user);
	}
	
	public List<User> searchByfromEmail(String from_email){
		return dao.searchByfromEmail(from_email);
	}
	


}