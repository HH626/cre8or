module.exports = {
	configureWebpack: {
		devtool: 'source-map'
	},
	outputDir: './dist'
}
