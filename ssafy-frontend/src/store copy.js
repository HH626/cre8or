import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
		accessToken: '',
		user: ''
  },
  getters: {
    getUser: function (state) {
      return state.user;
    }
  }
})
