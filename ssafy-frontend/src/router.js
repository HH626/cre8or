import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'


Vue.use(VueRouter)

const Home = () => import(/* webpackChunkName: "home" */ "./views/HomePage.vue")
const Login = () => import(/* webpackChunkName: "login" */ "./views/LoginPage.vue")
const Signup = () => import(/* webpackChunkName: "signup" */ "./views/SignUpPage.vue")
const Dashboard = () => import(/* webpackChunkName: "dashboard" */ "./views/DashboardPage.vue")
const Mypage = () => import(/* webpackChunkName: "mypage" */ "./views/MyPage.vue")
const Admin = () => import(/* webpackChunkName: "admin" */ './views/AdminPage.vue')
const AboutUs = () => import(/* webpackChunkName: "aboutus" */ './views/AboutUs.vue')
const Main = () => import(/* webpackChunkName: "welcome" */ './views/Welcome.vue')


const onlyAuthUser = (to, from, next) => {
  // 로그인이 안 된 유저는 로그인이 필요한 페이지(예: 마이페이지)로의 접속을 막아야 한다.
  const vueSession = JSON.parse(window.sessionStorage.getItem('vue-session-key'));
  if(!vueSession || !vueSession.user) {
    alert('Login required! redirect to home')
    next("/")
  } else {
    //alert(JSON.parse(window.sessionStorage.getItem('vue-session-key')).user.email+'님 반갑습니다!^^')
    next()
  }
}

const rejectAuthUser = (to, from, next) => {
  // 로그인이 된 유저가 다시 로그인 페이지로 접속하려고 할 때 막아야 한다.
  if (JSON.parse(window.sessionStorage.getItem('vue-session-key')).user) {
    alert('Already Logged in! You dont need to login again')
    next("/")
  } else {
    next()
  }
}

// const onlyAdminUser = (to, from, next) => {
//   // 관리자 페이지(component: Admin)에는 관리자로 등록 된 유저만 접속할 수 있다.
//   // if (store.state.user.live !== 'AD') {
//   // if (window.sessionStorage.getItem('check') !== 'AD'){
//   if (store.state.user.live !== 'AD'){
//     alert('접근 권한이 없습니다.')
//     next("/")
//   } else {
//     next()
//   }
// }

const routes =[
  // 사용자가 이상한 url 요청을 보낼 때 보내줄 곳은 홈으로 설정
  {
    path: '*',
    redirect: '/',
    // beforeEnter: alert('없는 경로입니다. 홈으로 이동합니다.')
  },

  // 직접 설정한 경로들
  {
    path: '/',
    name: "main",
    component: Main,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
  },
  {
    path: '/login',
    name: 'login',
    beforeEnter: rejectAuthUser,
    component: Login,
  },
  {
    path: '/signup',
    name: 'signup',
    component: Signup,
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
  },
  {
    path: '/mypage',
    name: 'mypage',
    beforeEnter: onlyAuthUser,
    component: Mypage,
  },
  {
    path: '/admin',
    name: 'admin',
    // beforeEnter: onlyAdminUser,
    component: Admin,
  },
  {
    path: '/aboutus',
    name: 'aboutus',
    component: AboutUs,
  },


]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
router.beforeEach( (to, from, next) => {
  next()
})
export default router
