import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
import VueSimplemde from 'vue-simplemde'
import FirebaseService from '@/services/FirebaseService'
import 'simplemde/dist/simplemde.min.css'
import 'font-awesome/css/font-awesome.min.css'
import '@mdi/font/css/materialdesignicons.css'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import moment from "moment"
import VueMomentJS from "vue-momentjs"

import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import VueSession from 'vue-session'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

Vue.use(VueSimplemde)
Vue.use(Vuetify)
Vue.use(VueSession)
Vue.use(BootstrapVue)
Vue.use(Chartkick.use(Chart))
Vue.use(FirebaseService);
Vue.use(VueMomentJS, moment)

// firebaseui css
require('../node_modules/firebaseui/dist/firebaseui.js')
require('../node_modules/firebaseui/dist/firebaseui.css')

new Vue({
  router,
	store,
	vuetify: new Vuetify({
		iconfont: 'mdi',
		theme: {
			primary: '#3f51b5',
			secondary: '#b0bec5',
			accent: '#8c9eff',
			error: '#b71c1c'
		}
	}),
	created() {
	},
  render: h => h(App)
}).$mount('#app')

const ignoreWarnMessage = 'The .native modifier for v-on is only valid on components but it was used on <div>.'
Vue.config.warnHandler = function (msg, vm, trace) {
	if (msg === ignoreWarnMessage) {
		msg = null;
		vm = null;
		trace = null
		
	}
}
