import Vue from 'vue'
import Vuex from 'vuex'
import http from "@/services/http-common";
import Constant from "./Constant.js";

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    dashboards: [],
    post: {},
    user: {},
    keywords:[],
    myPost:[],
    categories:[],
    myFavorites:[],
    likes:[],
    hostEmail : '',
    word:'',
    comments: []
  },

  getters: {
    //this.$store.getters.getUserEmail로 사용
    getUserEmail: function (state) {
      return state.user.user_email;
    }
    
  },

  mutations: {
    init(state, payload){

    },
    [Constant.GET_KEYWORDS]: (state, payload) => {
      state.keywords = payload.keywords.data;
    },
    [Constant.GET_DASHBOARDS]: (state, payload) => {
      state.dashboards = payload.dashboards;
    },
    [Constant.GET_DASHBOARDS_LIMIT]: (state, payload) => {
      var arr = payload.dashboards.data;
      for (let i = 0; i < arr.length; i++) {
        state.dashboards.push(arr[i]);
      }
      // state.dashboards = payload.dashboards;
    },
    [Constant.GET_DWMDASHBOARDS]: (state, payload) => {
      state.dashboards = payload.dashboards;
    },
    [Constant.GET_DWMDASHBOARDS_LIMIT]: (state, payload) => {
      var arr = payload.dashboards.data;
      for (let i = 0; i < arr.length; i++) {
        state.dashboards.push(arr[i]);
      }
    },
    [Constant.GET_USER]: (state, payload) => {
      state.user = payload.user;
    },
    [Constant.GET_CATEGORIES]: (state, payload) => {
      state.categories = payload.categories;
    },
    [Constant.GET_COMMENT]: (state, payload) => {
      state.comments = payload.comments;
    },
  },


  actions: {
    ///키워드가져오기
    [Constant.GET_KEYWORDS]: (context, payload) => {
      var cd = new Date()
      var date=cd.getFullYear()+ '-' + (cd.getMonth()+1) + '-' +cd.getDate()
      console.log(date)
      http.get(`/keyword/searchByTheDate/`+date)
        .then(response => {
          context.commit(Constant.GET_KEYWORDS, {
            keywords: response.data
          })
        }).catch(error => {
        });
    },

    //// 카테고리 가져오기
    [Constant.GET_CATEGORIES]: (context, payload) => {
      http.get("/category/searchAll")
      .then(response => {
        context.commit(Constant.GET_CATEGORIES, {
          categories: response.data.data
        })
      }).catch(err => {
      })
    },
    //// 게시글 작성
    [Constant.ADD_POST]: (store, payload) => {
      var post = payload.post;
      http.post("/post/addPost/", {
        title: post.title,
        content: post.content,
        user_email: post.user_email,
        keyword_no: post.keyword_no,
        file_name: post.file_name,
        file_type: post.file_type,
        category: post.category
      })
        .then(response => {
          if (response.data.state == "ok") {
          } else {
            alert("등록 실패");
          }
        })
        .catch((error) => {
          alert("error: " + error.message);
        });
    },
    //// 게시글 수정
    [Constant.UPDATE_POST]: (store, payload) => {
      http.put("post/update", {
        post_no: payload.post_no,
        title: payload.title,
        content: payload.content,
        user_email: payload.user_email,
        keyword_no: payload.keyword_no,
        file_name: payload.file_name,
        file_type: payload.file_type,
        category: payload.category
      })
      .then(response => {
        if(response.data.state == "ok") {
          //alert('수정 완료');
        }
        else {
          alert('게시글 수정 실패');
        }
      })
      .catch((error) => {
      })
    },
    //// 게시글 목록 조회
    [Constant.GET_DASHBOARDS]: store => {
      http
        .get(`/post/searchAll`)
        .then(response =>
          store.commit(Constant.GET_DASHBOARDS, {
            dashboards: response.data
          })
        )
        .catch(error => {
        });
    },
    // 게시글 10개씩 조회
    [Constant.GET_DASHBOARDS_LIMIT]: (store, payload) => {
      http
        .get(`/post/searchLimit/`+ payload.limit)
        .then(response => store.commit(Constant.GET_DASHBOARDS_LIMIT, {
          dashboards: response.data
        })
        )
        .catch(error => {
        });
    },
    // 일/주/월간 게시글 목록 조회
    [Constant.GET_DWMDASHBOARDS]: (store, payload) => {
      return new Promise((reslove, reject) => {
      http
        .get(`/post/searchSection/` + payload.section)
        .then(response => {
          store.commit(Constant.GET_DWMDASHBOARDS, {
            dashboards: response.data
          });
          reslove();
        })
        .catch(error => {
          reject();
        });
      })
    },
    // 일/주/월간 게시글 목록 10개씩 조회
    [Constant.GET_DWMDASHBOARDS_LIMIT]: (store, payload) => {
      return new Promise((reslove, reject) => {
      http
        .get(`/post/searchSectionLimit/` + payload.section + '/' + payload.limit)
        .then(response => {
          store.commit(Constant.GET_DWMDASHBOARDS_LIMIT, {
            dashboards: response.data
          });
          reslove();
        })
        .catch(error => {
          reject();
        });
      })
    },
    //// 게시글 삭제
    [Constant.DELETE_POST]: (store, payload) => {
      http.delete('/post/delete/'+payload.post_no)
      .then(response => {
        if(response.data.state == "ok") {

        }
        else {
          alert('게시글 삭제 실패');
        }
      })
      .catch(error => {
      })
    },
    //// 회원 가입
    [Constant.ADD_USER]: (store, payload) => {
      http
      .post("/user/addUser/", {
        user_email: payload.user_email,
        nickname: payload.nickname
      })
      .then(response => {
        if (response.data.state == "ok") {
        } else {
          alert("회원 등록 실패");
        }
      })
      .catch((error) => {
        alert("store:: 회원 가입 에러>> " + error.message);
      });
    },
    //// 회원 정보 조회
    [Constant.GET_USER]: (store, payload) => {
      return new Promise((resolve, reject) => {
        http.get("/user/getUser/" + payload.email)
          .then(response => {
            store.commit(Constant.GET_USER, { user: response.data.data });
            resolve();
          })
          .catch(error => {
            reject();
          });
      })
    },
    //// 회원정보 수정
    [Constant.UPDATE_USER]: (store, payload) => {
      http.put("/user/update", {
        user_email: payload.user.user_email,
        nickname: payload.user.nickname,
        profile_comment: payload.user.profile_comment,
        profile_img:payload.user.profile_img,
      })
        .then(response => {
          if (response.data.state == "ok") {
          } else {
            alert("수정 실패");
          }
        })
        .catch((error) => {
          alert("error: " + error.message);
        });
    },

    //// 즐겨찾기 추가
    [Constant.ADD_FOLLOW_USER]: (store, payload) => {
      http.post("/mypage/follow/addFollowUser", {
	      from_email : payload.from_email,
	      to_email : payload.to_email
        })
        .then(response => {
          if (response.data.state == "ok") {
          } else {
            alert("추가 실패");
          }
        })
        .catch((error) => {
          alert("error: " + error.message);
        });
    },
        //// 즐겨찾기 삭제
        [Constant.DELETE_FOLLOW_USER]: (store, payload) => {
          http.delete("/mypage/follow/deleteFollow", {
            data:{
              follow_no : 0,
              from_email : payload.from_email,
              to_email : payload.to_email.user_email
            }
          })
            .then(response => {
              if (response.data.state == "ok") {
              } else {
                alert("삭제 실패");
              }
            })
            .catch((error) => {
              alert("error: " + error.message);
            });
        },
    
    //// 댓글 등록
    [Constant.ADD_COMMENT]: (store, payload) => {
      var comment = payload.comment;
      http.post("/comment/insert", {
        content: comment.content,
        post_no: comment.post_no,
        user_email: comment.user_email
      })
      .then(response => {
        if(response.data.state == "ok") {

        }
        else {
        }
      })
      .catch((error) => {
      })
    },
    //// 댓글 조회
    [Constant.GET_COMMENT]: (store, payload) => {
      return new Promise((resolve, reject) => {
        http.get("/comment/searchAll/" + payload.post_no)
            .then(response => {
              store.commit(Constant.GET_COMMENT, { comments: response.data.data });
              resolve();
            })
            .catch(error => {
              reject();
            })
      })
    },
    //// 댓글 수정
    [Constant.EDIT_COMMENT]: (store, payload) => {
      http
      .put("/comment/update", {
        comment_no: payload.comment_no,
        content: payload.content
      })
      .then(response => {
        if(response.data.state == 'ok') {

        }
        else {
        }
      })
      .catch((error) => {
      })
    },
    //// 댓글 삭제
    [Constant.DELETE_COMMENT]: (store, payload) => {
      http.
      delete("/comment/delete/"+payload.comment_no)
      .then(response => {
        if(response.data.state == 'ok') {

        }
        else {
        }
      })
      .catch((error) => {
      })
    }
  },



});

export default store;