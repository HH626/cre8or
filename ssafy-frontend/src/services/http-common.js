import axios from 'axios'

export default 

	axios.create({
		
		// baseURL: 'https://ec2-15-165-15-232.ap-northeast-2.compute.amazonaws.com:8443',
		baseURL: 'http://localhost:8080',
		withCredentials: false,
		headers: {
			// "Content-Type": "application/x-www-form-urlencoded",
			'Accept': 'application/json',
			'Content-Type': 'application/json',

		}
	})

