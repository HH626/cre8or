import firebase, { app } from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const POSTS = 'posts'
const PORTFOLIOS = 'portfolios'

// SB web app's Firebase configuration
// Setup Firebase
// Do change to your own firebase configuration
// const firebaseConfig = {
// 	apiKey: "AIzaSyCcrhJeHGR2q5T7bSG4h1jwbgZQFXnhrTU",
// 	authDomain: "first-project-94be6.firebaseapp.com",
// 	databaseURL: "https://first-project-94be6.firebaseio.com",
// 	projectId: "first-project-94be6",
// 	storageBucket: "first-project-94be6.appspot.com"
// }
// firebase.initializeApp(firebaseConfig)


// EK web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyAQcDuePSnmmdWecgcYpxkfg_qMsSWiRQk",
	authDomain: "cre8or-2a293.firebaseapp.com",
	databaseURL: "https://cre8or-2a293.firebaseio.com",
	projectId: "cre8or-2a293",
	storageBucket: "cre8or-2a293.appspot.com",
	messagingSenderId: "760192275657",
	appId: "1:760192275657:web:1a1a9ab7a20c6ac02d2918"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// firebase.auth().onAuthStateChanged(function(user) {
// 	if(!app) {
// 		app = new Vue({
// 			el: '#app',
// 			template: '<App/>',
// 			components: { App },
// 			router
// 		})
// 	}
// })

const firestore = firebase.firestore()

export default {
	getPosts() {
		const postsCollection = firestore.collection(POSTS)
		return postsCollection
				.orderBy('created_at', 'desc')
				.get()
				.then((docSnapshots) => {
					return docSnapshots.docs.map((doc) => {
						let data = doc.data()
						data.created_at = new Date(data.created_at.toDate())
						return data
					})
				})
	},
	postPost(title, body) {
		return firestore.collection(POSTS).add({
			title,
			body,
			created_at: firebase.firestore.FieldValue.serverTimestamp()
		})
	},
	getPortfolios() {
		const postsCollection = firestore.collection(PORTFOLIOS)
		return postsCollection
				.orderBy('created_at', 'desc')
				.get()
				.then((docSnapshots) => {					
					return docSnapshots.docs.map((doc) => {
						let data = doc.data()
						data.created_at = new Date(data.created_at.toDate())
						return data
					})
				})
	},
	postPortfolio(title, body, img) {
		return firestore.collection(PORTFOLIOS).add({
			title,
			body,
			img,
			created_at: firebase.firestore.FieldValue.serverTimestamp()
		})
	},
	loginWithGoogle() {
		let provider = new firebase.auth.GoogleAuthProvider()
		return firebase.auth().signInWithPopup(provider).then(function(result) {
			let accessToken = result.credential.accessToken
			let user = result.user
			return result
		}).catch(function(error) {
			console.error('[Google Login Error]', error)
		})
	}
	
	
}